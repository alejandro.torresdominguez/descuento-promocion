class promo{
    constructor(precio,descuento){
        this.precio = precio;
        this.descuento = descuento;
    }
    get calculo(){
        return this.total();
    }
    total(){
        var des = this.descuento/100;
        var des2 = this.precio * des;
        return this.precio - des2;
    }
}

const resultado = new promo(1500,15);
console.log(resultado.calculo);